Alter table employees
drop employeesId;

Alter table employees
Add column employeesId int(10) primary KEY AUTO_INCREMENT;


INSERT INTO employees (firstName,lastName,dateOfBirth,phoneNumber,email,salary)
VALUES 
('Julie', 'Juliette', '1990-01-01', '0-800-900-111', 'julie@juliette.com', 5000),
('Sofie', 'Sophia', '1987-02-03', '0-800-900-222', 'sofie@sophia.com', 1700);

CREATE TABLE departments(
departmentId INT(10) NOT NULL AUTO_INCREMENT,
 name VARCHAR(25) NOT NULL,
 PRIMARY KEY (departmentId)
);

INSERT INTO departments (name)
VALUES 
('HR'),
('Finance');

alter table employees
add departmentId int(10) NULL;

Update employees set departmentId = 1 where employeesId = 1;
Update employees set departmentId = 2 where employeesId = 4;

INSERT INTO departments
VALUES 
(1,'HR'),
(2,'Finance');


ALTER TABLE employees
ADD CONSTRAINT fk_employee_department
FOREIGN KEY (departmentId) REFERENCES departments(departmentId);

delete from departments where departmentId = 1;

Update employees set departmentId = 10 where employeesId = 5;

Update employees set departmentId = 1 where employeesId = 5;

delete from employees where employeesId = 5;
