SELECT * FROM employees;

SELECT firstName , lastName FROM employees;

SELECT * FROM employees WHERE lastName = 'Johnson';

SELECT * FROM employees WHERE lastName LIKE 'J%';

SELECT * FROM employees WHERE lastName LIKE '%so%';

SELECT * FROM employees WHERE dateOfBirth > '1980-12-31';

SELECT * FROM employees WHERE dateOfBirth > '1980-12-31' AND firstName = 'John';

SELECT * FROM employees WHERE dateOfBirth > '1980-12-31' OR firstName = 'John';

SELECT * FROM employees WHERE NOT lastName = 'John';

SELECT MAX(salary) from employees;

SELECT MIN(salary) from employees;

SELECT AVG(salary) from employees;