Alter table employees
add managerId int(11) DEFAULT NULL;

ALTER TABLE employees
ADD CONSTRAINT fk_employee_employee
FOREIGN KEY (managerId) REFERENCES employees(employeesId);

Update employees set managerId = 6 where employeesId = 1;
Update employees set managerId = 6 where employeesId = 2;

CREATE TABLE projects (
  projectId int(11) NOT NULL AUTO_INCREMENT,
  description varchar(255) DEFAULT NULL,
  PRIMARY KEY (projectId)
);

INSERT INTO projects (description)
VALUES 
('Python - Cinema Web App'),
('Java - Fitness Web App');


CREATE TABLE employees_projects (
  employeeProjectId int(11) NOT NULL AUTO_INCREMENT,
  employeesId int(11) NOT NULL,
  projectId int(11) NOT NULL,
  PRIMARY KEY (employeeProjectId),
  KEY fk_employeeProject_employees (employeesId),
  KEY fk_employeeProject_projects (projectId),
  CONSTRAINT fk_employeeProject_employees FOREIGN KEY (employeesId) REFERENCES employees (employeesId),
  CONSTRAINT fk_employeeProject_projects FOREIGN KEY (projectId) REFERENCES projects (projectId)
);

INSERT INTO employees_projects (employeesId,projectId) VALUES (1,1),(2,1),(4,2),(6,2);

